from django import forms
from .models import MataKuliah
from django.contrib.auth.models import User

class MatkulForm(forms.ModelForm):
    class Meta:
        model = MataKuliah
        fields = [
            'nama',
            'dosen',
            'sks',
            'deskripsi',
            'semester_tahun',
            'ruang_kelas'
        ]

class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = [
            'username',
            'password',
            'email',
            'first_name',
            'last_name'
        ]

class SignUpForm(forms.ModelForm):
    class Meta:
        model = User
        fields = [
            'username',
            'password',
        ]