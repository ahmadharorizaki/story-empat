from django.contrib import auth
from django.http import response, JsonResponse
from django.shortcuts import render, redirect

from .forms import MatkulForm, UserForm, SignUpForm
from .models import MataKuliah, Peserta
import requests, random
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout

def home(request):
    return render(request, 'main/home.html')

def biodata(request):
    return render(request, 'main/biodata.html')

def ori(request):
    return render(request, 'main/ori.html')

def matkul(request):
    form = MatkulForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('/detail_matkul/')
    context = {
        'form' : form,
    }
    return render(request, "main/matkul.html", context)

def list_matkul(request):
    matkuls = MataKuliah.objects.all()
    context = {
        'matkuls' : matkuls
    }
    return render(request, "main/list_matkul.html", context)


def detail_matkul(request, my_id):
    matkul = MataKuliah.objects.get(id = my_id)
    context = {
        'matkul' : matkul
    }
    return render(request, "main/detail_matkul.html", context)

def delete_matkul(request, my_id):
    matkul = MataKuliah.objects.get(id = my_id)
    if request.method == "POST":
        matkul.delete()
        return redirect('../../')
    context={
        'matkul' : matkul
    }
    return render(request, "main/delete_matkul.html", context)

def kegiatan(request):
    response = {}
    return render(request, "main/kegiatan.html", response)

def buku(request):
    return render(request, 'main/buku.html')

def initial(request):
    words = ['universitas', 'indonesia', 'ui', 'fakultas', 'ilmu', 'komputer']
    chosen_word = random.choice(words)
    url = "https://www.googleapis.com/books/v1/volumes?q=" + chosen_word
    json_data = requests.get(url).json()
    return JsonResponse(json_data, safe=False)

def data(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    json_data = requests.get(url).json()
    return JsonResponse(json_data, safe=False)

def logIn(request):
    if request.method == "POST":
        user = authenticate(request, username=request.POST['username'], password=request.POST['password'])
        if user is not None:
            login(request, user)
            return redirect('/buku/')
        # Redirect to a success page.
        else:
            return redirect('/login/')
        # Return an 'invalid login' error message.
        # login(request, user)
        # return redirect('/buku/')
    else: 
        return render(request, "main/login.html")

def signup(request):
    form = UserForm(request.POST or None)
    if form.is_valid():
        user = form.save()
        login(request, user)
        return redirect('/buku/')
    context = {
        'form' : form,
    }
    return render(request, "main/signup.html", context)

def logOut(request):
    logout(request)
    return redirect("/buku/")