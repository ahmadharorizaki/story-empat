from django.http import response
from django.test import LiveServerTestCase, TestCase, tag
from django.test.client import Client
from django.urls import reverse
from django.urls.base import resolve
from selenium import webdriver
from .views import kegiatan, list_matkul, buku, data, initial, signup, logOut
from .models import Kegiatan, MataKuliah, Peserta
from django.contrib.auth.models import User


# @tag('functional')
# class FunctionalTestCase(LiveServerTestCase):
#     """Base class for functional test cases with selenium."""

#     @classmethod
#     def setUpClass(cls):
#         super().setUpClass()
#         # Change to another webdriver if desired (and update CI accordingly).
#         options = webdriver.chrome.options.Options()
#         # These options are needed for CI with Chromium.
#         options.headless = True  # Disable GUI.
#         options.add_argument('--no-sandbox')
#         options.add_argument('--disable-dev-shm-usage')
#         cls.selenium = webdriver.Chrome(options=options)

#     @classmethod
#     def tearDownClass(cls):
#         cls.selenium.quit()
#         super().tearDownClass()


class MainTestCase(TestCase):
    def test_root_url_status_200(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        # You can also use path names instead of explicit paths.
        response = self.client.get(reverse('main:home'))
        self.assertEqual(response.status_code, 200)

    # def test_apakah_url_tersedia(self):
    #     response = Client().get('/matkul/')
    #     self.assertEqual(response.status_code, 200)

    # def test_apakah_ada_method_views_yang_handle_matkul(self):
    #     found = resolve('/matkul/')
    #     self.assertEqual(found.func , matkul)

    def test_apakah_menggunakan_template_home(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'main/home.html')
    
    def test_apakah_menggunakan_template_biodata(self):
        response = Client().get('/biodata')
        self.assertTemplateUsed(response, 'main/biodata.html')
    
    def test_apakah_menggunakan_template_ori(self):
        response = Client().get('/ori')
        self.assertTemplateUsed(response, 'main/ori.html')

    def test_apakah_menggunakan_template_matkul(self):
        response = Client().get('/matkul/')
        self.assertTemplateUsed(response, 'main/matkul.html')

    def test_apakah_menggunakan_method_list_matkul(self):
        found = resolve('/detail_matkul/')
        self.assertEqual(found.func, list_matkul)
    
    def test_apakah_mengembalikan_template_detail_matkul(self):
        matkul = MataKuliah.objects.create(nama = 'PPW', dosen='Ori', sks=3, deskripsi='mantap', semester_tahun = 'GASAL 2020/2021', ruang_kelas='3114')
        response = Client().get('/detail_matkul/1/')
        self.assertTemplateUsed(response, 'main/detail_matkul.html')

    def test_apakah_terdapat_url_kegiatan(self):
        response = Client().get('/kegiatan/')
        self.assertEqual(response.status_code, 200)

    def test_apakah_menggunakan_template_kegiatan(self):
        response = Client().get('/kegiatan/')
        self.assertTemplateUsed(response, 'main/kegiatan.html')

    def test_apakah_menggunakan_method_kegiatan(self):
        found = resolve('/kegiatan/')
        self.assertEqual(found.func, kegiatan)

    def test_apakah_bisa_membuat_model_peserta(self):
        peserta = Peserta.objects.create(nama = 'Frans', kegiatan = Kegiatan.objects.create(nama = 'Asistensi'))
        self.assertEqual(Peserta.objects.all().count(), 1)
        
    def test_apakah_bisa_membuat_model_kegiatan(self):
        kegiatan = Kegiatan.objects.create(nama = 'Asistensi')
        self.assertEqual(Kegiatan.objects.all().count(), 1)

    def test_apakah_terdapat_accordion_berisikan_aktivitas_pengalaman_organisasi_kepanitiaan_prestasi(self):
        response = Client().get('')
        html_response = response.content.decode('utf8')
        self.assertIn("Aktivitas", html_response)
        self.assertIn("Pengalaman Organisasi", html_response)
        self.assertIn("Pengalaman Kepanitiaan", html_response)
        self.assertIn("Prestasi", html_response)

    def test_apakah_terdapat_url_buku(self):
        response = Client().get("/buku/")
        self.assertEqual(response.status_code, 200)
    
    def test_apakah_terdapat_fungsi_buku(self):
        found = resolve('/buku/')
        self.assertEqual(found.func, buku)
    
    def test_apakah_terdapat_url_data(self):
        response = Client().get("/data")
        self.assertEqual(response.status_code, 301)
    
    def test_apakah_terdapat_fungsi_data(self):
        found = resolve('/data/')
        self.assertEqual(found.func, data)

    def test_apakah_terdapat_url_initial(self):
        response = Client().get("/initial/")
        self.assertEqual(response.status_code, 200)

    def test_apakah_terdapat_fungsi_initial(self):
        found = resolve('/initial/')
        self.assertEqual(found.func, initial)
    

    def test_apakah_terdapat_url_signup(self):
        response = Client().get("/signup/")
        self.assertEqual(response.status_code, 200)

    def test_apakah_terdapat_fungsi_signup(self):
        found = resolve('/signup/')
        self.assertEqual(found.func, signup)

    def test_apakah_berhasil_signup(self):
        response = Client().post(
            '/signup/',
            {'username': 'orioriorioriori',
            'password': 'yamanonaka17',
            'email': 'ahmad.harori@ui.ac.id',
            'first_name': 'Ahmad',
            'last_name': 'Harori'}
            )
        user = User.objects.get(username='orioriorioriori')
        self.assertEqual(user.username, 'orioriorioriori')
    
    def test_apakah_terdapat_url_logout(self):
        response = Client().get("/logout/")
        self.assertRedirects(response, '/buku/')

    def test_apakah_terdapat_fungsi_logout(self):
        found = resolve('/logout/')
        self.assertEqual(found.func, logOut)

# class MainFunctionalTestCase(FunctionalTestCase):
#     def test_root_url_exists(self):
#         self.selenium.get(f'{self.live_server_url}/')
#         html = self.selenium.find_element_by_tag_name('html')
#         self.assertNotIn('not found', html.text.lower())
#         self.assertNotIn('error', html.text.lower())
