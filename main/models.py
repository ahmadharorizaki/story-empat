from django.db import models
from django.db.models.fields.related import ForeignKey
from django.urls import reverse

# Create your models here.

class MataKuliah(models.Model):

    nama = models.CharField(max_length=20)
    dosen = models.CharField(max_length=20)
    sks = models.IntegerField()
    deskripsi = models.TextField()
    semester_tahun = models.CharField(max_length=20)
    ruang_kelas = models.CharField(max_length=20)

    def get_absolute_url(self):
        return f"/detail_matkul/{self.id}/"

class Kegiatan(models.Model):
    nama = models.CharField(max_length=30)

class Peserta(models.Model):
    nama = models.CharField(max_length=30)
    kegiatan = ForeignKey(Kegiatan, on_delete=models.CASCADE, default=None)
