from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('biodata', views.biodata, name='biodata'),
    path('ori', views.ori, name='ori'),
    path('matkul/', views.matkul, name="matkul"),
    path('detail_matkul/', views.list_matkul, name="list_matkul"),
    path('detail_matkul/<int:my_id>/',views.detail_matkul, name="detail_matkul"),
    path('detail_matkul/<int:my_id>/delete/', views.delete_matkul, name="detail_matkul_delete"),
    path('kegiatan/', views.kegiatan, name="kegiatan"),
    path('buku/', views.buku, name="buku"),
    path('initial/', views.initial, name="inital"),
    path('data/', views.data, name="data"),
    path('signup/', views.signup, name="signup"),
    path('logout/', views.logOut, name="logout"),
    path('login/', views.logIn, name='login')
]
